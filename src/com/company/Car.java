package com.company;

public class Car {
    private String brand;
    private String model;
    private int price;
    private String version;
    private String transmission;

    public Car(String brand, String model, int price, String version, String transmission) {
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.version = version;
        this.transmission = transmission;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public String getVersion() {
        return version;
    }

    public String getTransmission() {
        return transmission;
    }
}
