package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MyCar {
    public static void main(String[] args) {
        List<Car> luxuryCars = new ArrayList<Car>() {{
            add(new Car("Mercedes", "S W222", 290000, "comfort", "automatic"));
            add(new Car("Audi", "A6 C7", 200000, "comfort", "automatic"));
            add(new Car("BMW", "X4", 220000, "comfort", "automatic"));
            add(new Car("Porsche", "Cayenne II", 340000, "comfort", "automatic"));
        }};
        List<Car> standardCards = new ArrayList<>() {{
            add(new Car("Opel", "Meriva", 56000, "standard", "automatic"));
            add(new Car("Opel", "Zafira", 80000, "standard", "manual"));
            add(new Car("Volkswagen", "Polo",  40000, "standard", "automatic"));
        }};
        luxuryCars.stream().mapToInt(a -> a.getPrice() * luxuryCars.size()).average().ifPresent(System.out::println);
        Integer
    }
}
